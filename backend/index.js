const express = require ('express');
const router = require('router');
const dotenv = require('dotenv');
const cors = require('cors');
const helmet = require('helmet');


// Configuration d'express
const app = express();

// Config dotenv
dotenv.config();

// Config cors
app.use(cors());

// Config d'Helmet
app.use(helmet());


app.get('/', (req, res) => {
    res.send("Hello World");
});

app.listen( process.env.PORT || 3001, () => {
    console.log(`Serveur démarré à l'adresse http://localhost:${process.env.PORT}`);
});